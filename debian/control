Source: yaha
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Homepage: https://github.com/GregoryFaust/yaha
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/med-team/yaha
Vcs-Git: https://salsa.debian.org/med-team/yaha.git

Package: yaha
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: find split-read mappings on single-end queries
 yaha is an open source, flexible, sensitive and accurate DNA aligner
 designed for single-end reads. It supports three major modes of
 operation:
  * The default “Optimal Query Coverage” (-OQC) mode reports the
    best set of alignments that cover the length of each query.
  * Using “Filter By Similarity” (-FBS), along with the best set of
    alignments, yaha will also output alignments that are highly similar
    to an alignment in the best set.
  * Finally, yaha can output all the alignments found for each query.
 The -OQC and -FBS modes are specifically tuned to form split read
 mappings that can be used to accurately identify structural variation
 events (deletions, duplications, insertions or inversions) between the
 subject query and the reference genome.
